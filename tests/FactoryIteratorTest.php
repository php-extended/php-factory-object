<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-factory-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Factory\AbstractFactory;
use PhpExtended\Factory\FactoryIterator;
use PHPUnit\Framework\TestCase;

/**
 * FactoryIteratorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Factory\FactoryIterator
 *
 * @internal
 *
 * @small
 */
class FactoryIteratorTest extends TestCase
{
	
	/**
	 * The iterator to test.
	 * 
	 * @var FactoryIterator
	 */
	protected FactoryIterator $_iterator;
	
	public function testToString() : void
	{
		$object = $this->_iterator;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testIteration() : void
	{
		$counter = 0;
		
		foreach($this->_iterator as $key => $object)
		{
			$this->assertEquals($counter, $key);
			$this->assertEquals(new stdClass(), $object);
			$counter++;
		}
		$this->assertEquals(2, $counter);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$factory = $this->createMock(AbstractFactory::class);
		$factory->expects($this->any())
			->method('create')
			->willReturn(new stdClass())
		;
		$this->_iterator = new FactoryIterator($factory, 2);
	}
	
}
