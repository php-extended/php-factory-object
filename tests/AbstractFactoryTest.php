<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-factory-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Factory\AbstractFactory;
use PhpExtended\Factory\FactoryIterator;
use PHPUnit\Framework\TestCase;

/**
 * AbstractFactoryTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Factory\AbstractFactory
 *
 * @internal
 *
 * @small
 */
class AbstractFactoryTest extends TestCase
{
	
	/**
	 * The factory to test.
	 * 
	 * @var AbstractFactory
	 */
	protected AbstractFactory $_factory;
	
	public function testToString() : void
	{
		$object = $this->_factory;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testCreateArray() : void
	{
		$this->assertEquals([new stdClass(), new stdClass()], $this->_factory->createArray(2));
	}
	
	public function testCreateIterator() : void
	{
		$this->assertInstanceOf(FactoryIterator::class, $this->_factory->createIterator(2));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = $this->getMockForAbstractClass(AbstractFactory::class);
		$this->_factory->expects($this->any())
			->method('create')
			->willReturn(new stdClass())
		;
	}
	
}
