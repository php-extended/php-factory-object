<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-factory-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Factory;

use Iterator;

/**
 * AbstractFactory class file.
 * 
 * This class is a generic implementation to be extended of the FactoryInterface.
 * 
 * @author Anastaszor
 * @template T of object
 * @implements FactoryInterface<T>
 */
abstract class AbstractFactory implements FactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::createArray()
	 */
	public function createArray(int $quantity = 1) : array
	{
		$objects = [];
		
		for($i = 0; $i < $quantity; $i++)
		{
			$objects[] = $this->create();
		}
		
		return $objects;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::createIterator()
	 */
	public function createIterator(int $maxQuantity = -1) : Iterator
	{
		return new FactoryIterator($this, $maxQuantity);
	}
	
}
