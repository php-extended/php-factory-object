<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-factory-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Factory;

use Iterator;
use Stringable;

/**
 * FactoryIterator class file.
 * 
 * This class is an iterator over objects that are created on the fly by a
 * factory.
 * 
 * @author Anastaszor
 * @template T of object
 * @implements \Iterator<integer, T>
 */
class FactoryIterator implements Iterator, Stringable
{
	
	/**
	 * The factory that generates objects.
	 * 
	 * @var FactoryInterface<T>
	 */
	protected FactoryInterface $_factory;
	
	/**
	 * The max quantity of objects this iterator must generate. Negative
	 * values are unlimited.
	 * 
	 * @var integer
	 */
	protected int $_maxQuantity = -1;
	
	/**
	 * The current key of the current object.
	 * 
	 * @var integer
	 */
	protected int $_key = 0;
	
	/**
	 * Creates a new FactoryIterator based.
	 * 
	 * @param FactoryInterface<T> $factory
	 * @param integer $maxQuantity
	 */
	public function __construct(FactoryInterface $factory, int $maxQuantity = -1)
	{
		$this->_factory = $factory;
		$this->_maxQuantity = $maxQuantity;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::current()
	 */
	public function current() : object
	{
		return $this->_factory->create();
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::next()
	 */
	public function next() : void
	{
		$this->_key++;
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::key()
	 */
	public function key() : int
	{
		return $this->_key;
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::valid()
	 */
	public function valid() : bool
	{
		return 0 > $this->_maxQuantity || $this->_key < $this->_maxQuantity;
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::rewind()
	 */
	public function rewind() : void
	{
		$this->_key = 0;
	}
	
}
