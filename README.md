# php-extended/php-factory-object

A library that implements the php-extended/php-factory-interface library.

![coverage](https://gitlab.com/php-extended/php-factory-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-factory-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-factory-object ^8`


## Basic Usage

This library provide a single class, the `AbstractFactory` which is
made to be implemented. Its `create` method must return a single object each
time it is called, whether that object is constant or contains some form of
deterministic input or randomness is left to the implementation.


## License

MIT (See [license file](LICENSE)).
